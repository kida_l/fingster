package com.valdio.valdioveliu.butterknifecalculator;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;

import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.io.Console;
import java.io.File;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;


public class MainActivity extends ActionBarActivity {

    int colors[] = {R.layout.red_round_button, R.layout.green_round_button, R.layout.blue_round_button};
    //Bind the Views of the activity_main.xml file

    
    @Bind(R.id.button1)
    Button button1;
    @Bind(R.id.button2)
    Button button2;
    @Bind(R.id.button3)
    Button button3;
    @Bind(R.id.button4)
    Button button4;
    @Bind(R.id.button5)
    Button button5;
    @Bind(R.id.button6)
    Button button6;
    @Bind(R.id.button7)
    Button button7;
    @Bind(R.id.button8)
    Button button8;
    @Bind(R.id.button9)
    Button button9;
    @Bind(R.id.button10)
    Button button10;
    @Bind(R.id.button11)
    Button button11;
    @Bind(R.id.button12)
    Button button12;
    @Bind(R.id.go_button)
    Button goButton;



     //The onClick() methods for the buttons


     @OnClick(R.id.back)
     public void onClick() {
         long mills = 1000L;
         Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
         vibrator.vibrate(mills);
     }
     @OnClick(R.id.button1)
     public void onClick1() {
        append(button1);
     }
     @OnClick(R.id.button2)
     public void onClick2() {
         append(button2);
     }
     @OnClick(R.id.button3)
     public void onClick3() {
         append(button3);
     }
     @OnClick(R.id.button4)
     public void onClick4() {
         append(button4);
     }
     @OnClick(R.id.button5)
     public void onClick5() {
         append(button5);
     }
     @OnClick(R.id.button6)
     public void onClick6() {
         append(button6);
     }
     @OnClick(R.id.button7)
     public void onClick7() {
         append(button7);
     }
     @OnClick(R.id.button8)
     public void onClick8() {
         append(button8);
     }
     @OnClick(R.id.button9)
     public void onClick9() {
         append(button9);
     }
     @OnClick(R.id.button10)
     public void onClick10() {
         append(button10);
     }
    @OnClick(R.id.button11)
    public void onClick11() {
        append(button11);
    }
    @OnClick(R.id.button12)
    public void onClick12() {
        append(button12);
    }
     @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
     @OnTouch(R.id.go_button)
     public boolean onClickGoButton() {
         Random rn = new Random();
         int n = 5 - 10 + 1;
         int tick = rn.nextInt() % n + 5;
         for (int i = 0, num = 1; i < tick; i++, num++) {
             if(num==4) num = 1;
             //File back = new File("/app/res/layout/" + colors[num-1] + "_round_button.xml");
             //goButton.setBackground(Drawable.createFromPath(Uri.fromFile(back)));
            // goButton.setBackground(colors[num-1]);
             goButton.setBackgroundResource(colors[num-1]);
             goButton.setText(Integer.toString(num));
             SystemClock.sleep(100);
             //try { SystemClock.sleep(1000); }
             //catch (InterruptedException ex) { android.util.Log.d("YourApplicationName", ex.toString()); }

         }
         return false;

     }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    public void append(Button btn){


        Log.d("button: ", btn.getText().toString());
        Log.d("button: ", goButton.getText().toString());
        if(!btn.getText().toString().equals(goButton.getText().toString())) {
            long mills = 1000L;
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(mills);

        }

     }

}
